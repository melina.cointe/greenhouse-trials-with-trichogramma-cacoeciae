# Greenhouse trials with Trichogramma cacoeciae

This project contains all the different files used to obtain the results and figures presented in Cointe et al. 2023. "Study of the dispersal of two batches of Trichogramma cacoeciae strains in greenhouses".

An R script called "GreenhouseScript_2" with all the codes from the analyses to the different graphs.

And two csv files. One entitled "ALLWEEKS_HEATMappy.csv" to obtain the data and graphs presented for the biotic and abiotic parameters (temperature, hygrometry, plant size)

A second file entitled "DataSerres_3.csv" lists all the data concerning the two years of experimentation and serves as a basis for the analyses carried out in the R script.


## License

Gnu general public license
